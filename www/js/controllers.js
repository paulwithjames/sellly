angular.module('starter.controllers', ['ionic','ion-gallery'])

.controller('MainCtrl', function($scope, sellAPI, $ionicModal, $state, $ionicNavBarDelegate) {
	
	
})
.controller('DashCtrl', function($scope, sellAPI, $ionicModal, $state, $ionicNavBarDelegate) {
  $.support.cors = true;

  $scope.gopost = function(){
	$state.go("tab.upload");
  }
  
	$ionicNavBarDelegate.showBar(true);
	$ionicNavBarDelegate.showBackButton(false);
  $scope.chooseFriends = function(referid){
	  sellAPI.getFriends(referid).then(
		function(data){
			console.log(data);
			$scope.friends = data.data.data.friends;
			$ionicModal.fromTemplateUrl('templates/friend-pass.html', {
				scope: $scope,
				animation: 'slide-in-up'
			  }).then(function(modal) {


			   $scope.modal = modal;
			   $scope.modal.show();
							  //$scope.modal.modalinit();
							  //$ionicLoading.hide();
							});

				}
			  )
  }

  $scope.forward = function(referid){

	for(var i = 0; i < $scope.friends.length; i++){
		if($scope.friends[i].isChecked){
			sellAPI.forward($scope.friends[i]._id,referid).then(
			function(data){
				console.log(data);
				$scope.modal.remove();
			},
			function(error){
				console.log("error");
			}
			)
		}

	}

  }



/*
  $scope.login = function(){
      //alert("hey");
	 $scope.me = {};
    sellAPI.login('paolo@qwikwire.com','abc').then(
      function(data){
        //alert("hey");
        $scope.me = data.data.data;
        console.log(data);
      }
    )

  }

  $scope.login();
*/

$scope.closeModal2 = function() {
// $scope.modal.hide();
// alert('closeme');
$scope.modal2.remove();
};

$scope.closeModal = function() {
// $scope.modal.hide();
// alert('closeme');
$scope.modal.remove();
};


  $scope.showModal = function(item){
    //$scope.activeImage = 'http://selly.1000laurel.com/uploads/' + image;
    $scope.item = item;
    $ionicModal.fromTemplateUrl('templates/image.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {


       $scope.modal2 = modal;
       $scope.modal2.show();
                      //$scope.modal.modalinit();
                      //$ionicLoading.hide();
                    });
  }

  $scope.items = [];
  $scope.getReferrals = function(){
    sellAPI.getReferrals().then(
      function(data){
        console.log(data);
        var items = data.data.data.items;
        $scope.items = [];
        $scope.items2 = data.data.data.items;
        for(var i=0; i < items.length; i++){
          var item = {};
          item.src = 'http://selly.1000laurel.com/uploads/' + items[i].pics[0];
          item.sub = items[i].title;
          $scope.items.push(item);
        }
        console.log($scope.items);
       // $scope.items.apply();
      },
	  function(error){
		$state.go("tab.login");
      }
    )
  }
  $scope.init = function(){
	$scope.getReferrals();
  }

    /*$scope.items = [
      {
        src: 'img/shoes.jpg',
        sub: 'This is a shoes'
      },
      {
        src: 'img/bags.jpg',
         sub: 'This is a bags'
      },

      {
        src: 'img/bags.jpg',
         sub: 'This is a bags'
      },

      {
        src: 'img/shoes.jpg',
        sub: 'This is a shoes'
      }

    ]*/
})


.controller('NavCtrl', function($scope, $ionicSideMenuDelegate) {
  $scope.showMenu = function () {
    $ionicSideMenuDelegate.toggleLeft();
  };
  $scope.showRightMenu = function () {
    $ionicSideMenuDelegate.toggleRight();
  };
})

.controller('FriendsCtrl', function($scope, Friends) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.friends = Friends.all();
  $scope.remove = function(friend) {
    Friends.remove(friend);
  };
})

.controller('FriendDetailCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.friendId);
})

.controller('SideMenu', function($scope) {
 $scope.shouldShowDelete = false;
 $scope.shouldShowReorder = false;
 $scope.listCanSwipe = true
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})


.controller('UploadCtrl', function($scope, Upload, sellAPI, $ionicModal) {
        $scope.myitem = {};
        $scope.myitem.qwois = 30;
       //$scope.qwoins = 30;
       $scope.cropper = {};
       $scope.cropper.sourceImage = null;
       //$scope.bounds = {};

       $scope.forward = function(referid){

       for(var i = 0; i < $scope.friends.length; i++){
         if($scope.friends[i].isChecked){
           sellAPI.forward($scope.friends[i]._id,referid).then(
           function(data){
             console.log(data);
             $scope.modal.remove();
           }
           )
         }

       }

       }


       if($scope.myitem.meetup){
         $scope.myitem.mee = "E";
       }else{
         $scope.myitem.mee = "";
       }

       $scope.upload = function(){
         Upload.upload({

           url: 'http://selly.1000laurel.com/api/post/item',
           data: {
             image: Upload.dataUrltoBlob($scope.cropper.croppedImage,'gloop-image'),
             description: $scope.myitem.description,
             title: $scope.myitem.title,
            price:  $scope.myitem.price,
            qwoins:  $scope.myitem.qwoins,
             meetup:  $scope.myitem.mee,
             origin:  $scope.myitem.origin
           }
         }).then(function (response) {
          //win();


          sellAPI.getFriends(response.data.referralid).then(
      		function(data){
      			console.log(data);
      			$scope.friends = data.data.data.friends;
      			$ionicModal.fromTemplateUrl('templates/friend-pass.html', {
      				scope: $scope,
      				animation: 'slide-in-up'
      			  }).then(function(modal) {


      			   $scope.modal = modal;
      			   $scope.modal.show();
      							  //$scope.modal.modalinit();
      							  //$ionicLoading.hide();
      							});

      				}
      			  )

        }, function (response) {
          //fail();
        }, function (evt) {
         $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
       })
       }


      /* $scope.cropper.croppedImage   = null;

       $scope.bounds.left = 0;
       $scope.bounds.right = 0;
       $scope.bounds.top = 0;
       $scope.bounds.bottom = 0;*/
})

.controller('ViewItemCtrl', function($scope) {
})


.controller('AddFriendCtrl', function($scope, sellAPI, $ionicPopup) {
	$scope.friend ={};
	$scope.addfriend = function(){
		console.log($scope.friend);
		sellAPI.inviteFriend({email:$scope.friend.email})
		.then(
			function(data){
			   var alertPopup = $ionicPopup.alert({
				 title: 'Friend Request',
				 template: 'Wait for your friend to accept your invitation'
			   });

			   alertPopup.then(function(res) {
				 //console.log('Thank you for not eating my delicious ice cream cone');
			   });					
			}
		);
	};

})

.controller('LoginCtrl', function($scope, $ionicNavBarDelegate) {
$ionicNavBarDelegate.showBar(false);
})

.controller('PaymentCtrl', function($scope) {

})

.controller('ChatsCtrl', function($scope, Friends) {

  $scope.friends = Friends.all();
  $scope.remove = function(friend) {
    Friends.remove(friend);
  };
})

.controller('ChatCtrl', function($scope, $stateParams, Friends) {
  $scope.friend = Friends.get($stateParams.chatId);
})


.controller('FriendRequestsCtrl', function($scope, $stateParams, sellAPI) {
  sellAPI.getInvites()
  .then(function(data){
	  $scope.friends = data.data.data.requests;
	});
  
  $scope.accept = function(ix){
	sellAPI.confirmFriend({request:$scope.friends[ix].requestid})
	.then(function(data){
		$scope.friends.splice(ix,1);
	});
  };
  $scope.remove = function(ix) {
    $scope.friends.splice(ix,1);
  };
});
