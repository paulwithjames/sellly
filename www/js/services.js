angular.module('starter.services', [])

.constant('ApiEndpoint', {
  url: 'http://selly.1000laurel.com/api'
})

.config(function($sceDelegateProvider) {
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://selly.1000laurel.com/**'
  ]);

  // The blacklist overrides the whitelist so the open redirect here is blocked.
  $sceDelegateProvider.resourceUrlBlacklist([
    'http://myapp.example.com/clickThru**'
  ]);
})

.service('sellAPI', function($http, $q, ApiEndpoint) {

  //var pre = "http://atingdaan.ronrontron.com/api/"; //device
//  var pre = "http://onewatt.xyz/onewattapi/"; //prod



 //var pre = "/daan/"; //test
 //var pre = "http://atingdaan.ronrontron.com/api/"; //prod
  var pre = ApiEndpoint.url; //prod
 //this.imgpre = "http://atingdaan.ronrontron.com/images/";
 
 this.globals = {
  project : {}
}

this.login = function(email, password){
  var request = $http
  ({
   method: "POST",
   url: pre + "/login",
   data:{
     email: email,
     password: password
   }
 });
  return request;
};

this.getReferrals = function(){
  var request = $http
  ({
   method: "GET",
   url: pre + "/get/referals"
 });
  return request;
};

this.forward = function(userid,referid){
	var request = $http
  ({
   method: "POST",
   url: pre + "/forward/referral",
   data:{
	   userid: userid,
	   referralid: referid
   }
 });
  return request;
}

this.getFriends = function(itemid){
	var request = $http
  ({
   method: "GET",
   url: pre + "/get/friends",
   params:{
	   itemid: itemid
   }
 });
  return request;
}

this.inviteFriend = function(data){
	var request = $http
  ({
   method: "POST",
   url: pre + "/add/friend",
   data:data
 });
  return request;
}

this.getInvites = function(data){
	var request = $http
  ({
   method: "GET",
   url: pre + "/get/friendrequests"
 });
  return request;
}

this.confirmFriend = function(data){
	var request = $http
  ({
   method: "POST",
   url: pre + "/confirm/friend",
   data:data
 });
  return request;
}
})

.factory('Friends', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var friends = [{
    id: 0,
    name: 'Ben Sparrow',
    face: 'img/ben.png',
    products: [
      {
        id: 0,
        name: "Techmania"
      },
      {
        id: 1,
        name: "Glukgluk"
      },
      {
        id: 2,
        name: "Genekom"
      }
    ],
  }, {
    id: 1,
    name: 'Max Lynx',
    face: 'img/max.png',
    products: [
      {
        id: 0,
        name: "Techmania"
      },
      {
        id: 1,
        name: "Glukgluk"
      },
      {
        id: 2,
        name: "Genekom"
      }
    ],
  }, {
    id: 2,
    name: 'Adam Bradleyson',
    face: 'img/adam.jpg',
    products: [
      {
        id: 0,
        name: "Techmania"
      },
      {
        id: 1,
        name: "Glukgluk"
      },
      {
        id: 2,
        name: "Genekom"
      }
    ],
  }, {
    id: 3,
    name: 'Perry Governor',
    face: 'img/perry.png',
    products: [
      {
        id: 0,
        name: "Techmania"
      },
      {
        id: 1,
        name: "Glukgluk"
      },
      {
        id: 2,
        name: "Genekom"
      }
    ],
  }, {
    id: 4,
    name: 'Mike Harrington',
    products: [
      {
        id: 0,
        name: "Techmania"
      },
      {
        id: 1,
        name: "Glukgluk"
      },
      {
        id: 2,
        name: "Genekom"
      }
    ],
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return friends;
    },
    remove: function(friend) {
      friends.splice(friends.indexOf(friend), 1);
    },
    get: function(friendId) {
      for (var i = 0; i < friends.length; i++) {
        if (friends[i].id === parseInt(friendId)) {
          return friends[i];
        }
      }
      return null;
    }
  };
});
